import numpy as np

global density_matrix_computed
density_matrix_computed = None

def compute_density(lattice_matrix, compute_anyways=True):
	global density_matrix_computed

	if density_matrix_computed is None or compute_anyways is True:
		density_matrix_computed = np.sum(lattice_matrix, axis=2)

	return density_matrix_computed