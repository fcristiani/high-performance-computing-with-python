import numpy as np
from ghost_cells_manager import GhostCellsManager
from cartcomm_shifter import CartcommShifter
import mpi_file_writer

class LatticeCommunicator:
	def __init__(self, comm, dims):
		self.comm = comm

		self.cartcomm = self.comm.Create_cart(dims, periods=False)
		cartcommShifter = CartcommShifter(self.cartcomm)

		self.src_dest_values = cartcommShifter.shift()
		
		self.ghostCellsManager = GhostCellsManager(self.src_dest_values)

	def write_to_file(self, file_name, value):
		mpi_file_writer.save_mpiio(
			self.cartcomm,
			file_name,
			value
		)

	def add_ghost_cells(self, lattice_matrix):
		return self.ghostCellsManager.add_ghost_cells(lattice_matrix)

	def remove_ghost_cells(self, lattice_matrix):
		return self.ghostCellsManager.remove_ghost_cells(lattice_matrix)

	def communicate(self, lattice_matrix):
		lattice_matrix = self.communicate_right(lattice_matrix)
		lattice_matrix = self.communicate_up(lattice_matrix)
		lattice_matrix = self.communicate_left(lattice_matrix)
		lattice_matrix = self.communicate_down(lattice_matrix)

		return lattice_matrix

	def communicate_up(self, lattice_matrix):
		(src_shift_up, dest_shift_up) = self.src_dest_values["up"]

		self.comm.Sendrecv(
			sendbuf=lattice_matrix[1, :, :],
			dest=dest_shift_up,
			recvbuf=lattice_matrix[-1, :, :],
			source=src_shift_up
		)

		return lattice_matrix

	def communicate_down(self, lattice_matrix):
		(src_shift_down, dest_shift_down) = self.src_dest_values["down"]

		self.comm.Sendrecv(
			sendbuf=lattice_matrix[-2, :, :],
			dest=dest_shift_down,
			recvbuf=lattice_matrix[0, :, :],
			source=src_shift_down
		)

		return lattice_matrix

	def communicate_right(self, lattice_matrix):
		(src_shift_right, dest_shift_right) = self.src_dest_values["right"]

		shift_right_sendbuf = np.copy(lattice_matrix[:, -2, :])
		shift_right_recvbuf = np.copy(lattice_matrix[:, 0, :])
		
		self.comm.Sendrecv(
			sendbuf=shift_right_sendbuf,
			dest=dest_shift_right,
			recvbuf=shift_right_recvbuf,
			source=src_shift_right
		)

		lattice_matrix[:, -2, :] = shift_right_sendbuf
		lattice_matrix[:, 0, :] = shift_right_recvbuf

		return lattice_matrix

	def communicate_left(self, lattice_matrix):
		(src_shift_left, dest_shift_left) = self.src_dest_values["left"]

		shift_left_sendbuf = np.copy(lattice_matrix[:, 1, :])
		shift_left_recvbuf = np.copy(lattice_matrix[:, -1, :])
		
		self.comm.Sendrecv(
			sendbuf=shift_left_sendbuf,
			dest=dest_shift_left,
			recvbuf=shift_left_recvbuf,
			source=src_shift_left
		)

		lattice_matrix[:, 1, :] = np.copy(shift_left_sendbuf)
		lattice_matrix[:, -1, :] = np.copy(shift_left_recvbuf)

		return lattice_matrix
