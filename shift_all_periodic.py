import numpy as np
import channel


def shift(lattice_matrix):
	for id_channel in range(1, channel.get_number_of_channels()): # Rest channel is excluded from shifting action
		lattice_matrix[:, :, id_channel] = np.roll(
			lattice_matrix[:, :, id_channel],
			channel.get_velocity_vector(id_channel),
			(0, 1)
		)

	return lattice_matrix