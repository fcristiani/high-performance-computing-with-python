import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import density
import velocity

import sys

plt.close('all')
plt.ion()

if len(sys.argv) >= 2:
	omega = float(sys.argv[1])
else:
	omega = 0.8

if len(sys.argv) >= 3:
	v_zero = float(sys.argv[2])
else:
	v_zero = 0.1

print("Omega: {}".format(omega))
print("V_zero: {}".format(v_zero))

lattice_rows = 10
lattice_columns = 15

rho_max = 1
rho = rho_max * np.ones((lattice_rows, lattice_columns))

u = velocity.get_sinusoidal_vx_velocity_matrix(
	v_zero,
	(lattice_rows, lattice_columns),
	epsilon_percentage=40
)
v_max = np.max(u)

lattice_matrix = scatter.get_f_eq(rho, u)

plt.figure(figsize=(11,6))

i = 1
while(True):
	plt.suptitle("Iteration: {} | Omega: {}".format(i, omega))
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

	density_matrix = density.compute_density(lattice_matrix)

	velocity_matrix = velocity.compute_velocity(
		lattice_matrix,
		density_matrix
	)

	rho_min = np.min(density_matrix)

	print("Density: [{0:.5f}, {1:.5f}]".format(rho_min, rho_max))

	plots.plot_density_and_velocity(
		density_matrix,
		rho_max,
		rho_min,
		velocity_matrix,
		v_max * 12
	)

	i += 1