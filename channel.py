import numpy as np

velocities = np.array(
	[
		[0, 0], # Rest channel
		[0, 1], # Channel 1: East
		[1, 0], # Channel 2: North
		[0, -1], # Channel 3: West
		[-1, 0], # Channel 4: South
		[1, 1], # Channel 5: North East
		[1, -1], # Channel 6: North West
		[-1, -1], # Channel 7: South West
		[-1, 1], # Channel 8: South East
	]
)

w = np.array([4/9, 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36])

def get_velocities():
	return velocities

def get_w():
	return w

def get_number_of_channels():
	return len(velocities)

def get_velocity_vector(id_channel):
	return velocities[id_channel]

def get_velocity_norms():
	return np.sqrt((velocities[:, 0])**2 + (velocities[:, 1])**2)