import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import density
import velocity

import sys

plt.close('all')
plt.ion()

if len(sys.argv) == 2:
	omega = float(sys.argv[1])
else:
	omega = 0.8

lattice_rows = 10
lattice_columns = 15
l_x = np.arange(0, lattice_columns)

rho_zero = 0.25
epsilon_rho = (1 * rho_zero) / 100
rho_x = rho_zero + (epsilon_rho * np.sin((2 * np.pi * l_x) / (lattice_columns - 1)))
rho = (rho_x.T * np.ones((rho_x.shape[0], lattice_rows)).T).T
rho_max = rho_zero + epsilon_rho
rho_min = rho_zero - epsilon_rho
rho = rho.T

u_init = np.zeros((lattice_rows, lattice_columns, 2)) + 0
v_max = np.max(u_init)

if v_max == 0:
	v_max = 0.01

lattice_matrix = scatter.get_f_eq(rho, u_init)

plt.figure(figsize=(11,6))

i = 1
while(True):
	plt.suptitle("Iteration: {} | Omega: {}".format(i, omega))
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

	density_matrix = density.compute_density(lattice_matrix)

	velocity_matrix = velocity.compute_velocity(
		lattice_matrix,
		density_matrix
	)

	plots.plot_density_and_velocity(
		density_matrix,
		rho_max,
		rho_min,
		velocity_matrix,
		v_max * 15
	)

	i += 1