import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import density
import velocity
import viscocity

import sys

plt.close('all')

if len(sys.argv) >= 2:
	omega = float(sys.argv[1])
else:
	omega = 0.8

if len(sys.argv) >= 3:
	v_zero = float(sys.argv[2])
else:
	v_zero = 0.1

if len(sys.argv) >= 4:
	iterations = int(sys.argv[3])
else:
	iterations = 1000

lattice_rows = 10
lattice_columns = 15

rho_max = 1
rho = rho_max * np.ones((lattice_rows, lattice_columns))

u = velocity.get_sinusoidal_vx_velocity_matrix(
	v_zero,
	(lattice_rows, lattice_columns),
	epsilon_percentage=10
)

max_velocity = np.zeros(iterations)
coordinates_max_velocity = np.unravel_index(u.argmax(), u.shape)
max_velocity[0] = u[coordinates_max_velocity]

lattice_matrix = scatter.get_f_eq(rho, u)

for i in np.arange(iterations):
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

	density_matrix = density.compute_density(lattice_matrix)

	velocity_matrix = velocity.compute_velocity(
		lattice_matrix,
		density_matrix
	)

	max_velocity[i] = velocity_matrix[coordinates_max_velocity]

viscocity_value = viscocity.get_viscocity(max_velocity, lattice_rows)
velocity_with_viscocity = viscocity.generate_velocities_for_viscocity_like(
	viscocity_value,
	max_velocity,
	lattice_rows
)

print("Viscocity for omega {0}: {1:.2f}".format(omega, viscocity_value))

plt.figure()
plt.title("Viscocity for omega = {}".format(omega))

v_min = np.min(max_velocity)
first_index_of_minimum = np.min(np.where(max_velocity == v_min)[0]) // 2

t = np.arange(first_index_of_minimum)

plt.plot(t, max_velocity[t])
plt.plot(t, velocity_with_viscocity[t])

plt.legend(["Measured velocity", "Velocity with viscocity = {0:.2f}".format(viscocity_value)])

plt.show()