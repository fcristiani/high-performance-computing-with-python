import numpy as np
import scatter
import channel

def init(rows, columns):
	return 0.01 + np.round(np.random.rand(
		rows,
		columns,
		channel.get_number_of_channels())
	)

def scatter_shift(lattice_matrix, omega, shift_module=None):
	if not shift_module:
		import shift_all_periodic as shift
	else:
		shift = shift_module

	lattice_matrix = scatter.scatter(lattice_matrix, omega)
	lattice_matrix = shift.shift(lattice_matrix)
	return lattice_matrix

def get_average_difference_over_channels(lattice_matrix1, lattice_matrix2):
	diffs = np.ones(lattice_matrix1.shape[2])

	for id_channel in range(0, lattice_matrix1.shape[2]):
		diff = (lattice_matrix1[:, :, id_channel] - lattice_matrix2[:, :, id_channel])
		squared_diff = (1/(diff.shape[0] * diff.shape[1])) * np.sum(diff ** 2)
		diffs[id_channel - 1] = squared_diff

	return np.mean(diffs)