import lattice
import shift

lattice_matrix = lattice.init(15, 10)

number_of_channels = lattice_matrix.shape[2]
range_channels = range(1, number_of_channels) # Rest channel is excluded from shifting action

for id_channel in range_channels: # Rest channel is excluded from shifting action
	print("Lattice for channel: {} before shifting".format(id_channel))
	print(lattice_matrix[:, :, id_channel])

print("Shifting ...")
lattice_matrix = shift.shift(lattice_matrix)

for id_channel in range_channels: # Rest channel is excluded from shifting action
	print("Lattice for channel: {} after shifting".format(id_channel))
	print(lattice_matrix[:, :, id_channel])