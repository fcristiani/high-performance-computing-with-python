import numpy as np
import channel

def compute_velocity(lattice_matrix, density_matrix):
	c_times_f = np.dot(lattice_matrix, channel.get_velocities())
	inverse_density = 1 / density_matrix
	# inverse_density = density_matrix

	# We have to transpose every matrix in order to match dimensions
	# and to achieve element wise multiplication.
	return (c_times_f.T * inverse_density.T).T

def get_sinusoidal_vx_velocity_matrix(v_zero, lattice_shape, epsilon_percentage=15):
	lattice_rows = lattice_shape[0]
	lattice_columns = lattice_shape[1]
	l_y = np.arange(0, lattice_rows)

	u = np.zeros((lattice_rows, lattice_columns, 2))

	if v_zero != 0:
		epsilon_v = (epsilon_percentage * v_zero) / 100
	else:
		epsilon_v = 0.1

	velocities_x = v_zero + epsilon_v * np.sin((2 * np.pi * l_y) / (lattice_rows - 1))
	v_max = v_zero + epsilon_v
	u[:, :, 1] = (velocities_x.T * np.ones((velocities_x.shape[0], lattice_columns)).T).T

	return u