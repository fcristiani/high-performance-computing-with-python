class CartcommShifter:
	def __init__(self, cartcomm):
		self.cartcomm = cartcomm

	def shift(self):
		src_dest_values = {}

		src_dest_values["right"] = self.shift_right()
		src_dest_values["up"] = self.shift_up()
		src_dest_values["left"] = self.shift_left()
		src_dest_values["down"] = self.shift_down()

		return src_dest_values

	def shift_up(self):
		return self.cartcomm.Shift(0, -1)

	def shift_down(self):
		return self.cartcomm.Shift(0, 1)

	def shift_right(self):
		return self.cartcomm.Shift(1, 1)

	def shift_left(self):
		return self.cartcomm.Shift(1, -1)