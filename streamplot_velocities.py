import numpy as np
import matplotlib.pyplot as plt
import sys

filename_x = sys.argv[1]
filename_y = sys.argv[2]

print("Recovering from files: {} and {}".format(filename_x, filename_y))

ux = np.load(filename_x)
uy = np.load(filename_y)

velocity_matrix = np.zeros((ux.shape[0], ux.shape[1], 2))
velocity_matrix[:, :, 1] = ux
velocity_matrix[:, :, 0] = uy

X, Y = np.meshgrid(
	np.arange(0, velocity_matrix.shape[1]),
	np.arange(0, velocity_matrix.shape[0])
)

U = velocity_matrix[:, :, 1]
V = velocity_matrix[:, :, 0]

plt.streamplot(X, Y, U, V)
plt.show()