class ParallelPrinter:
	def print(self, comm, str, rank=0):
		if rank is None:
			print("Rank: {}".format(comm.Get_rank()))
			print(str)
		else:
			if comm.Get_rank() == rank:
				print(str)