import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import density

import sys

plt.close('all')
plt.ion()

if len(sys.argv) == 2:
	omega = float(sys.argv[1])
else:
	omega = 0.8

rho_max = 0.1
u_max = 0.05
lattice_rows = 10
lattice_columns = 15

rho_init = np.random.uniform(
	0,
	rho_max,
	(lattice_rows, lattice_columns)
)

u_init = (u_max * np.random.uniform(
	-u_max,
	u_max,
	(lattice_rows, lattice_columns, 2)
))

lattice_matrix = scatter.get_f_eq(rho_init, u_init)

plt.figure(figsize=(6,4))

i = 1
while(True):
	plt.suptitle("Iteration: {} | Omega: {}".format(i, omega))
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

	density_matrix = density.compute_density(lattice_matrix)

	plots.plot_density(density_matrix, rho_max)

	i += 1