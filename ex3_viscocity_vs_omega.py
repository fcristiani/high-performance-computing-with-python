import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import density
import velocity
import viscocity

plt.close('all')

omegas = np.linspace(0.5, 1.8, 100)
iterations = 10000

viscocities = np.zeros_like(omegas)

i = 0
for omega in omegas:
	lattice_rows = 10
	lattice_columns = 15

	rho_max = 1
	rho = rho_max * np.ones((lattice_rows, lattice_columns))

	v_zero = 0.1
	u = velocity.get_sinusoidal_vx_velocity_matrix(
		v_zero,
		(lattice_rows, lattice_columns),
		epsilon_percentage=5
	)

	max_velocity = np.zeros(iterations)
	coordinates_max_velocity = np.unravel_index(u.argmax(), u.shape)
	max_velocity[0] = u[coordinates_max_velocity]

	lattice_matrix = scatter.get_f_eq(rho, u)

	for j in np.arange(iterations):
		lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

		density_matrix = density.compute_density(lattice_matrix)

		velocity_matrix = velocity.compute_velocity(
			lattice_matrix,
			density_matrix
		)

		max_velocity[j] = velocity_matrix[coordinates_max_velocity]

	viscocity_value = viscocity.get_viscocity(max_velocity, lattice_rows)
	velocity_with_viscocity = viscocity.generate_velocities_for_viscocity_like(
		viscocity_value,
		max_velocity,
		lattice_rows
	)

	print("i: {0}, omega: {1:.4f}, viscocity: {2:.2f}".format(i, omega, viscocity_value))

	viscocities[i] = viscocity_value

	i += 1

omega_inverse = (1/omegas) - 1/2

c = np.mean(viscocities / omega_inverse)

print("Constant: {0:.4f}".format(c))

plt.figure(figsize=(10,6))
plt.suptitle("Viscocity vs omega (c = {0:.2f})".format(c))

plt.subplot(1, 2, 1)
plt.xlabel('omega')
plt.ylabel('viscocity')
plt.plot(omegas, viscocities)
plt.plot(omegas, c * omega_inverse)
plt.legend([
	"viscocity(omega)",
	"c * ((1/omega) - 0.5)"
])

plt.subplot(1, 2, 2)
plt.xlabel('(1/omega) - 0.5')
plt.plot(omega_inverse, viscocities)
plt.plot(omega_inverse, c * omega_inverse)
plt.legend([
	"viscocity(omega)",
	"c * ((1/omega) - 0.5)"
])

plt.show()