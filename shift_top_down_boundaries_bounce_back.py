import numpy as np
import channel
import shift_all_periodic
import density

def shift(lattice_matrix):
	rows = lattice_matrix.shape[0]
	columns = lattice_matrix.shape[1]

	buffer_channels_top_down_boundaries = np.zeros(
		(channel.get_number_of_channels(), columns)
	)

	u_boundary_x = -0.2
	u_boundary = np.array([0, u_boundary_x])

	for id_channel in range(1, channel.get_number_of_channels()):
		velocity_vector = channel.get_velocity_vector(id_channel)

		if id_channel == 2 or id_channel == 5 or id_channel == 6:
			buffer_channels_top_down_boundaries[id_channel, :] = lattice_matrix[-1, :, id_channel]

		elif id_channel == 4 or id_channel == 7 or id_channel == 8:
			buffer_channels_top_down_boundaries[id_channel, :] = lattice_matrix[0, :, id_channel]

		lattice_matrix[:, :, id_channel] = np.roll(
			lattice_matrix[:, :, id_channel],
			channel.get_velocity_vector(id_channel),
			(0, 1)
		)

	lattice_matrix = move_particles_to_other_channel(
		lattice_matrix,
		buffer_channels_top_down_boundaries,
		u_boundary
	)

	return lattice_matrix

def move_particles_to_other_channel(
	lattice_matrix,
	buffer_channels_top_down_boundaries,
	u_boundary
):
	density_matrix = density.compute_density(lattice_matrix, compute_anyways=False)

	for id_channel in range(1, channel.get_number_of_channels()):
		if id_channel == 2:
			lattice_matrix[0, :, id_channel] = buffer_channels_top_down_boundaries[4]

		elif id_channel == 4:
			# Channel 4 not affected because scalar product is c * u = 0.
			lattice_matrix[-1, :, id_channel] = buffer_channels_top_down_boundaries[2]

		elif id_channel == 5:
			lattice_matrix[0, :, id_channel] = buffer_channels_top_down_boundaries[7]

		elif id_channel == 6:
			lattice_matrix[0, :, id_channel] = buffer_channels_top_down_boundaries[8]

		elif id_channel == 7:
			alpha = get_alpha(id_channel, u_boundary, density_matrix)
			lattice_matrix[-1, :, id_channel] = buffer_channels_top_down_boundaries[5] - alpha

		elif id_channel == 8:
			alpha = get_alpha(id_channel, u_boundary, density_matrix)
			lattice_matrix[-1, :, id_channel] = buffer_channels_top_down_boundaries[6] - alpha

	return lattice_matrix

def get_alpha(id_channel, u_boundary, density_matrix):
	c = channel.get_velocities() # c.shape = (9, 2)
	c_times_u = np.dot(c[id_channel], u_boundary)

	density_boundary = density_matrix[-1, :]

	w = channel.get_w()

	return (6 * w[id_channel] * density_boundary * c_times_u)
