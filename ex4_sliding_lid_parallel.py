from mpi4py import MPI
import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import shift_sliding_lid_bounce_back as shift_module
import density
import velocity
import sys
from parallel_printer import ParallelPrinter
from lattice_communicator import LatticeCommunicator

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
omega = 1.7

printer = ParallelPrinter()

if len(sys.argv) != 3:
	printer.print(
		comm,
		"Dimensions missing. Usage:\n\t`mpirun -n X python {} [dims_rows] [dims_columns]`".format(__file__)
	)
	sys.exit(1)

dims_rows = int(sys.argv[1])
dims_columns = int(sys.argv[2])

dims = (dims_rows, dims_columns)

whole_lattice_rows = 2000
whole_lattice_columns = 2000
lattice_rows = whole_lattice_rows // dims_rows
lattice_columns = whole_lattice_columns // dims_columns

rho_max = 1
rho_init = np.ones((lattice_rows, lattice_columns))

u_init = np.zeros((lattice_rows, lattice_columns, 2))
u_max = 0.1
lattice_matrix = scatter.get_f_eq(rho_init, u_init)

latticeCommunicator = LatticeCommunicator(comm, dims)
lattice_matrix = latticeCommunicator.add_ghost_cells(lattice_matrix)

iterations = 150000
for i in range(0, iterations + 1):
	lattice_matrix = latticeCommunicator.communicate(lattice_matrix)
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega, shift_module=shift_module)

	if (i % 1000) == 0:
		printer.print(comm, "Iteration: {}".format(i))

lattice_matrix_without_ghost_cells = latticeCommunicator.remove_ghost_cells(lattice_matrix)
density_matrix = density.compute_density(lattice_matrix_without_ghost_cells)

velocity_matrix = velocity.compute_velocity(
	lattice_matrix_without_ghost_cells,
	density_matrix
)

file_name_velocity_x = "velocity_x__{}_by_{}__{}.npy".format(dims_rows, dims_columns, iterations)
latticeCommunicator.write_to_file(file_name_velocity_x, velocity_matrix[:, :, 1])

file_name_velocity_y = "velocity_y__{}_by_{}__{}.npy".format(dims_rows, dims_columns, iterations)
latticeCommunicator.write_to_file(file_name_velocity_y, velocity_matrix[:, :, 0])
