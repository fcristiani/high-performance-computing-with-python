import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt

omegas = np.array([0.01, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 1.9])
iterations = np.arange(0, 500)
rho_max = 0.5
u_max = 0.05
lattice_rows = 5
lattice_columns = 5

diffs = np.zeros_like(iterations, dtype=float)

labels = []

for omega in omegas:
	print("Omega: {}".format(omega))
	
	rho_init = np.random.uniform(
		0,
		rho_max,
		(lattice_rows, lattice_columns)
	)

	u_init = (u_max * np.random.uniform(
		-u_max,
		u_max,
		(lattice_rows, lattice_columns, 2)
	))

	lattice_matrix = scatter.get_f_eq(rho_init, u_init)

	lattice_matrix_prev = np.mean(lattice_matrix) * np.random.rand(
		lattice_matrix.shape[0],
		lattice_matrix.shape[1],
		lattice_matrix.shape[2]
	)

	for i in iterations:
		lattice_matrix = lattice.scatter_shift(lattice_matrix, omega)

		average_diff = lattice.get_average_difference_over_channels(
			lattice_matrix,
			lattice_matrix_prev
		)

		diffs[i] = average_diff

		lattice_matrix_prev = lattice_matrix

	print("Diff: {}".format(average_diff))
	plt.plot(diffs)
	labels.append("omega = {}".format(omega))
	pass

colormap = plt.cm.gist_ncar
plt.gca().set_prop_cycle(
	'color',
	[colormap(i) for i in np.linspace(0, 0.9, omegas.shape[0])]
)

plt.title('Average difference between iterations')

plt.legend(labels)
plt.show()