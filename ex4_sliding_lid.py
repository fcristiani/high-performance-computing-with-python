import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import shift_sliding_lid_bounce_back as shift_module
import density
import velocity

plt.close('all')
plt.ion()

omega = 1.7
lattice_rows = 300
lattice_columns = 300

rho_max = 1
rho_init = np.ones((lattice_rows, lattice_columns))

u_init = np.zeros((lattice_rows, lattice_columns, 2))
u_max = 0.1
lattice_matrix = scatter.get_f_eq(rho_init, u_init)

fig = plt.figure(figsize=(11,6))

i = 1
while(True):
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega, shift_module=shift_module)

	density_matrix = density.compute_density(lattice_matrix)

	velocity_matrix = velocity.compute_velocity(
		lattice_matrix,
		density_matrix
	)

	if (i % 50) == 0:
		number_of_points = lattice_rows * lattice_columns

		print("Iteration: {}".format(i))

		plots.plot_velocity_streamplot(
			velocity_matrix,
			title="Iteration: {}".format(i),
			save_to_file="../figures/ex4_sliding_lid/ex4_sliding_lid_{}.png".format(i)
		)

	i += 1