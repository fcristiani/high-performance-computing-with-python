import numpy as np
import lattice
import scatter
import matplotlib.pyplot as plt
import plots
import shift_top_down_boundaries_bounce_back as shift_module
import density
import velocity

plt.close('all')

omega = 0.5
iterations = 200
lattice_rows = 10
lattice_columns = 15

rho_max = 1
rho_init = np.ones((lattice_rows, lattice_columns))

u_init = np.zeros((lattice_rows, lattice_columns, 2))
u_max = 0.2
column_to_plot = lattice_columns // 2
lattice_matrix = scatter.get_f_eq(rho_init, u_init)

for i in np.arange(iterations):
	lattice_matrix = lattice.scatter_shift(lattice_matrix, omega, shift_module=shift_module)

	density_matrix = density.compute_density(lattice_matrix)

	velocity_matrix = velocity.compute_velocity(
		lattice_matrix,
		density_matrix
	)

plt.title("Velocity x component for middle column")
plt.plot(velocity_matrix[:, column_to_plot, 1])
plt.ylabel('Velocity (x component)')
plt.xlabel('Row')
plt.show()