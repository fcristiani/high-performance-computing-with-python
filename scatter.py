import numpy as np
import channel
import velocity
import density

def scatter(lattice_matrix, omega):
	rho = density.compute_density(lattice_matrix) # rho.shape = (rows, columns)
	u = velocity.compute_velocity(lattice_matrix, rho) # u.shape = (rows, columns, 2)
	
	f_eq = get_f_eq(rho, u)

	return lattice_matrix + (omega * (f_eq - lattice_matrix))

def get_f_eq(rho, u):
	c = channel.get_velocities() # c.shape = (9, 2)
	c_times_u = np.einsum('ij, klj->kli', c, u) # c_times_u.shape = (rows, columns, 9)
	c_times_u_square = c_times_u ** 2 # c_times_u_square.shape = (rows, columns, 9)
	u_square = (u ** 2).sum(axis=2)

	ones_three_dimensions = np.ones_like(c_times_u)
	u_square_with_3_dimensions = (ones_three_dimensions.T * u_square.T).T # u_square_with_3_dimensions.shape = (rows, columns, 9)

	A = ones_three_dimensions
	A += (3 * c_times_u)
	A += ((9/2) * c_times_u_square)
	A -= ((3/2) * u_square_with_3_dimensions)
	# A.shape = (rows, columns, 9)

	return channel.get_w() * ((A.T * rho.T).T)