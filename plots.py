import matplotlib.pyplot as plt
import numpy as np

def plot_channels(lattice_matrix):
	channels = np.array([6, 2, 5, 3, 0, 1, 7, 4, 8])

	vmin = np.min(lattice_matrix)
	vmax = np.max(lattice_matrix)

	position_subplot = 1
	for id_channel in channels:
		plt.subplot(3, 3, position_subplot)
		plt.pcolormesh(lattice_matrix[:, :, id_channel], vmin=vmin, vmax=vmax)
		plt.title('Lattice for channel: {}'.format(id_channel))
		position_subplot += 1

	plt.draw()
	cax = plt.axes([0.91, 0.1, 0.03, 0.8])	
	plt.subplots_adjust(left=0.02, right=0.89, top=0.89, bottom=0.1)
	plt.colorbar(cax = cax)
	plt.pause(0.0001)

def plot_density(density_matrix, max_value):
	plt.pcolormesh(density_matrix, vmax=max_value)

	plt.draw()
	plt.colorbar()
	plt.pause(0.0001)
	plt.clf()

def plot_velocity(velocity_matrix, max_value, title=None):
	X, Y = np.meshgrid(
		np.arange(0, velocity_matrix.shape[1]),
		np.arange(0, velocity_matrix.shape[0])
	)

	U = velocity_matrix[:, :, 1]
	V = velocity_matrix[:, :, 0]

	plt.title(title)
	plt.quiver(X, Y, U, V, units='width', scale=max_value)
	plt.draw()
	plt.pause(0.0001)
	plt.clf()

def plot_velocity_streamplot(velocity_matrix, title=None, save_to_file=None):

	X, Y = np.meshgrid(
		np.arange(0, velocity_matrix.shape[1]),
		np.arange(0, velocity_matrix.shape[0])
	)

	U = velocity_matrix[:, :, 1]
	V = velocity_matrix[:, :, 0]

	plt.title(title)
	plt.streamplot(X, Y, U, V)
	plt.draw()

	if save_to_file is not None:
		plt.savefig(save_to_file)

	plt.pause(0.01)
	plt.clf()

def plot_density_and_velocity(density_matrix, density_max_value, density_min_value, velocity_matrix, velocity_max_value, save_to_file=None):
	plt.subplot(1, 2, 1)
	plt.title('Density')
	plt.pcolormesh(
		density_matrix,
		vmax=density_max_value,
		vmin=density_min_value
	)
	plt.colorbar()

	plt.subplot(1, 2, 2)
	plt.title('Velocity')
	X, Y = np.meshgrid(
		np.arange(0, velocity_matrix.shape[1]),
		np.arange(0, velocity_matrix.shape[0])
	)

	U = velocity_matrix[:, :, 1]
	V = velocity_matrix[:, :, 0]

	plt.quiver(X, Y, U, V, units='width', scale=velocity_max_value)
	plt.draw()

	if save_to_file is not None:
		plt.savefig(save_to_file)

	plt.pause(0.0001)
	plt.clf()
