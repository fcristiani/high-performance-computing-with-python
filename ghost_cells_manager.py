import numpy as np

class GhostCellsManager:
	def __init__(self, src_dest_values):
		self.src_dest_values = src_dest_values

	def add_ghost_cells(self, lattice_matrix):
		lattice_matrix = self.add_ghost_cells_right(lattice_matrix)
		lattice_matrix = self.add_ghost_cells_up(lattice_matrix)
		lattice_matrix = self.add_ghost_cells_left(lattice_matrix)
		lattice_matrix = self.add_ghost_cells_down(lattice_matrix)

		return lattice_matrix

	def remove_ghost_cells(self, lattice_matrix):
		lattice_matrix = self.remove_ghost_cells_right(lattice_matrix)
		lattice_matrix = self.remove_ghost_cells_up(lattice_matrix)
		lattice_matrix = self.remove_ghost_cells_left(lattice_matrix)
		lattice_matrix = self.remove_ghost_cells_down(lattice_matrix)

		return lattice_matrix

	def add_ghost_cells_right(self, lattice_matrix):
		(src_shift_right, dest_shift_right) = self.src_dest_values["right"]

		if dest_shift_right != -2:
			ghost_zeros = np.zeros((lattice_matrix.shape[0], lattice_matrix.shape[1] + 1, lattice_matrix.shape[2]))
			ghost_zeros[:, :-1] = lattice_matrix
			lattice_matrix = ghost_zeros

		return lattice_matrix

	def add_ghost_cells_up(self, lattice_matrix):
		(src_shift_up, dest_shift_up) = self.src_dest_values["up"]

		if dest_shift_up != -2:
			ghost_zeros = np.zeros((lattice_matrix.shape[0] + 1, lattice_matrix.shape[1], lattice_matrix.shape[2]))
			ghost_zeros[1:] = lattice_matrix
			lattice_matrix = ghost_zeros

		return lattice_matrix

	def add_ghost_cells_left(self, lattice_matrix):
		(src_shift_left, dest_shift_left) = self.src_dest_values["left"]

		if dest_shift_left != -2:
			ghost_zeros = np.zeros((lattice_matrix.shape[0], lattice_matrix.shape[1] + 1, lattice_matrix.shape[2]))
			ghost_zeros[:, 1:] = lattice_matrix
			lattice_matrix = ghost_zeros

		return lattice_matrix

	def add_ghost_cells_down(self, lattice_matrix):
		(src_shift_down, dest_shift_down) = self.src_dest_values["down"]

		if dest_shift_down != -2:
			ghost_zeros = np.zeros((lattice_matrix.shape[0] + 1, lattice_matrix.shape[1], lattice_matrix.shape[2]))
			ghost_zeros[:-1] = lattice_matrix
			lattice_matrix = ghost_zeros

		return lattice_matrix

	def remove_ghost_cells_right(self, lattice_matrix):
		(src_shift_right, dest_shift_right) = self.src_dest_values["right"]

		if dest_shift_right != -2:
			lattice_matrix = lattice_matrix[:, :-1]

		return lattice_matrix

	def remove_ghost_cells_up(self, lattice_matrix):
		(src_shift_up, dest_shift_up) = self.src_dest_values["up"]

		if dest_shift_up != -2:
			lattice_matrix = lattice_matrix[1:]

		return lattice_matrix

	def remove_ghost_cells_left(self, lattice_matrix):
		(src_shift_left, dest_shift_left) = self.src_dest_values["left"]

		if dest_shift_left != -2:
			lattice_matrix = lattice_matrix[:, 1:]

		return lattice_matrix

	def remove_ghost_cells_down(self, lattice_matrix):
		(src_shift_down, dest_shift_down) = self.src_dest_values["down"]

		if dest_shift_down != -2:
			lattice_matrix = lattice_matrix[:-1]

		return lattice_matrix

