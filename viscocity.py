import numpy as np

def get_viscocity(velocities, L_y):
	iterations = velocities.shape[0]

	v_time_zero = velocities[0]
	v_min = np.min(velocities)
	first_index_of_minimum = np.min(np.where(velocities == v_min)[0]) // 2

	index_iteration_measure_velocity = first_index_of_minimum * 10 // 100
	v_specific_time = velocities[index_iteration_measure_velocity]

	# v_x = v_zero * exp(-kappa * t)
	# kappa = viscocity * (k ** 2)
	# kappa = viscocty * ((2pi / L_y) ** 2)

	kappa = (np.log(v_time_zero - v_min) - np.log(v_specific_time - v_min)) / index_iteration_measure_velocity

	viscocity_value = kappa * ((L_y - 1) ** 2) / ((2 * np.pi) ** 2)

	return viscocity_value

def generate_velocities_for_viscocity_like(
	viscocity_value,
	velocities,
	L_y
):
	offset = np.min(velocities)
	init_value = velocities[0]
	iterations = velocities.shape[0]

	kappa = viscocity_value * ((2 * np.pi / (L_y - 1)) ** 2)
	velocity_with_viscocity = (init_value - offset) * np.exp(-(kappa * np.arange(iterations)))
	velocity_with_viscocity += offset

	return velocity_with_viscocity