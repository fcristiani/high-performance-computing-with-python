import numpy as np
import matplotlib.pyplot as plt

timesInSeconds300x300 = np.array([
	7536.045,
	3612.065,
	1807.329,
	1018.611,
	708.681,
	598.290,
	519.207,
	509.538,
	447.663,
	284.222,
	257.436,
	211.893,
	178.439,
	171.308,
	168.208,
	162.994,
	161.77,
	158.508,
	162.319,
	161.007,
	160.889,
	190,
	181,
	195,
	229
])

timesInSeconds2000x2000 = np.array([
	540000,
	270000,
	124560,
	50580,
	47640,
	39480,
	8448.836,
	2955.863,
	1027.399,
	802.297,
	643.96,
	593.797,
	455.68,
	395.858,
	354.578,
	348.504,
	335.129
	
])

processes300x300 = np.array([
	1, # 1 x 1
	2, # 2 x 1
	4, # 2 x 2
	6, # 3 x 2
	9, # 3 x 3
	12, # 4 x 3
	16, # 4 x 4
	20, # 5 x 4
	25, # 5 x 5
	30, # 6 x 5
	36, # 6 x 6
	60, # 10 x 6
	100, # 10 x 10
	120, # 12 x 10
	144, # 12 x 12
	180, # 15 x 12
	225, # 15 x 15
	300, # 20 x 15
	400, # 20 x 20
	500, # 25 x 20
	625, # 25 x 25
	750, # 25 x 30
	900, # 30 x 30
	1500, # 30 x 50
	2500 # 50 x 50
])

processes2000x2000 = np.array([
	1, # 1 x 1
	2, # 2 x 1
	4, # 2 x 2
	16, # 4 x 4
	20, # 5 x 4
	25, # 5 x 5
	100, # 10 x 10
	200, # 10 x 20
	400,# 20 x 20
	500, # 25 x 20
	625, # 25 x 25
	750, # 25 x 30
	900, # 30 x 30
	1200, # 30 x 40
	1600, # 40 x 40
	2000, # 40 x 50
	2500 # 50 x 50
])

normalizedTimes300x300 = timesInSeconds300x300[0] / timesInSeconds300x300
normalizedTimes2000x2000 = timesInSeconds2000x2000[0] / timesInSeconds2000x2000

ideal = np.logspace(0, 3.5, num=20)

fig, ax = plt.subplots()
ax.loglog(processes300x300, normalizedTimes300x300)
plt.scatter(processes300x300, normalizedTimes300x300, marker=".")

ax.loglog(processes2000x2000, timesInSeconds2000x2000[0] / timesInSeconds2000x2000)
plt.scatter(processes2000x2000, normalizedTimes2000x2000, marker=".")

ax.loglog(ideal, ideal)
plt.scatter(ideal, ideal, marker=".")

plt.legend(["300x300 grid", "2000x2000 grid", "Ideal"])

ax.grid(True)
plt.ylabel("Speedup")
plt.xlabel("Number of parallel processes")
plt.title("Parallelization speedup")
plt.show()